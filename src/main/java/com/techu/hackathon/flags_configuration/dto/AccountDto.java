package com.techu.hackathon.flags_configuration.dto;

public class AccountDto {

    private String id;
    private String nroCuenta;
    private ProductDto producto;
    private String codigoCliente;
    private double saldoDisponible;
    private double saldoConsumido;
    private String moneda;
    private Boolean activo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public ProductDto getProducto() {
        return producto;
    }

    public void setProducto(ProductDto producto) {
        this.producto = producto;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public double getSaldoDisponible() {
        return saldoDisponible;
    }

    public void setSaldoDisponible(double saldoDisponible) {
        this.saldoDisponible = saldoDisponible;
    }

    public double getSaldoConsumido() {
        return saldoConsumido;
    }

    public void setSaldoConsumido(double saldoConsumido) {
        this.saldoConsumido = saldoConsumido;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }
}
