package com.techu.hackathon.flags_configuration.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.core.mapping.Document;

public class ProductDto {

    private String tipo;
    private String nombre;

    public ProductDto(String tipo, String nombre){
        this.setTipo(tipo);
        this.setNombre(nombre);
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
