package com.techu.hackathon.flags_configuration.exception;

public class ResourceException extends Exception{

    public ResourceException(String message) {
        super(message);
    }
}
