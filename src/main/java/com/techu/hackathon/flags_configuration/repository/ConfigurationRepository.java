package com.techu.hackathon.flags_configuration.repository;

import com.techu.hackathon.flags_configuration.model.ConfigurationModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ConfigurationRepository{

    ConfigurationModel save(ConfigurationModel configuration);

    ConfigurationModel findByCode(String code);

    List<ConfigurationModel> findAll();

    ConfigurationModel update(ConfigurationModel configuration);
}
