package com.techu.hackathon.flags_configuration.repository;

import com.techu.hackathon.flags_configuration.model.ConfigurationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ConfigurationRepositoryImpl implements ConfigurationRepository{

    private final MongoOperations mongoOperations;

    @Autowired
    public ConfigurationRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public ConfigurationModel save(ConfigurationModel configuration) {
        this.mongoOperations.save(configuration);
        return this.findByCode(configuration.getFlagCode());
    }

    @Override
    public ConfigurationModel findByCode(String code) {
        return this.mongoOperations.findOne(new Query(Criteria.where("flagCode").is(code)), ConfigurationModel.class);
    }

    @Override
    public List<ConfigurationModel> findAll() {
        return this.mongoOperations.findAll(ConfigurationModel.class);
    }

    @Override
    public ConfigurationModel update(ConfigurationModel configuration) {
        this.mongoOperations.save(configuration);
        return this.findByCode(configuration.getFlagCode());
    }
}
