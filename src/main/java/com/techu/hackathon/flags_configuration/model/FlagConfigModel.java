package com.techu.hackathon.flags_configuration.model;

public class FlagConfigModel {

    private int flagNumber;
    private String flagText;

    public FlagConfigModel() {
    }

    public FlagConfigModel(int flagNumber, String flagText) {
        this.flagNumber = flagNumber;
        this.flagText = flagText;
    }

    public int getFlagNumber() {
        return flagNumber;
    }

    public void setFlagNumber(int flagNumber) {
        this.flagNumber = flagNumber;
    }

    public String getFlagText() {
        return flagText;
    }

    public void setFlagText(String flagText) {
        this.flagText = flagText;
    }
}
