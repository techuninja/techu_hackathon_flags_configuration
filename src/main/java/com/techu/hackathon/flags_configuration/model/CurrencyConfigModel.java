package com.techu.hackathon.flags_configuration.model;

public class CurrencyConfigModel {

    private String currencyCode;

    public CurrencyConfigModel() {
    }

    public CurrencyConfigModel(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
