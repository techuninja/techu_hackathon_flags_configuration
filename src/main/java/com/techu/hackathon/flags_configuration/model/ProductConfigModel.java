package com.techu.hackathon.flags_configuration.model;

public class ProductConfigModel {

    private String productoName;

    public ProductConfigModel() {
    }

    public ProductConfigModel(String productoName) {
        this.productoName = productoName;
    }

    public String getProductoName() {
        return productoName;
    }

    public void setProductoName(String productoName) {
        this.productoName = productoName;
    }
}
