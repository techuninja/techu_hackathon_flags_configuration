package com.techu.hackathon.flags_configuration.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "flags")
public class ConfigurationModel {

    @Id
    @NotNull
    private String id;

    @NotNull
    private String flagCode;

    private List<ProductConfigModel> productEnabled;

    private List<ProductConfigModel> productNotEnabled;

    private List<CurrencyConfigModel> currencyEnabled;

    private List<CurrencyConfigModel> currencyNotEnabled;

    private List<BalanceConfigModel> balanceAvailableRequired;

    private List<BalanceConfigModel> balanceAvailableNotRequired;

    private FlagConfigModel flagOk;

    private FlagConfigModel flagNoOk;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlagCode() {
        return flagCode;
    }

    public void setFlagCode(String flagCode) {
        this.flagCode = flagCode;
    }

    public List<ProductConfigModel> getProductEnabled() {
        return productEnabled;
    }

    public void setProductEnabled(List<ProductConfigModel> productEnabled) {
        this.productEnabled = productEnabled;
    }

    public List<ProductConfigModel> getProductNotEnabled() {
        return productNotEnabled;
    }

    public void setProductNotEnabled(List<ProductConfigModel> productNotEnabled) {
        this.productNotEnabled = productNotEnabled;
    }

    public List<CurrencyConfigModel> getCurrencyEnabled() {
        return currencyEnabled;
    }

    public void setCurrencyEnabled(List<CurrencyConfigModel> currencyEnabled) {
        this.currencyEnabled = currencyEnabled;
    }

    public List<CurrencyConfigModel> getCurrencyNotEnabled() {
        return currencyNotEnabled;
    }

    public void setCurrencyNotEnabled(List<CurrencyConfigModel> currencyNotEnabled) {
        this.currencyNotEnabled = currencyNotEnabled;
    }

    public List<BalanceConfigModel> getBalanceAvailableRequired() {
        return balanceAvailableRequired;
    }

    public void setBalanceAvailableRequired(List<BalanceConfigModel> balanceAvailableRequired) {
        this.balanceAvailableRequired = balanceAvailableRequired;
    }

    public List<BalanceConfigModel> getBalanceAvailableNotRequired() {
        return balanceAvailableNotRequired;
    }

    public void setBalanceAvailableNotRequired(List<BalanceConfigModel> balanceAvailableNotRequired) {
        this.balanceAvailableNotRequired = balanceAvailableNotRequired;
    }

    public FlagConfigModel getFlagOk() {
        return flagOk;
    }

    public void setFlagOk(FlagConfigModel flagOk) {
        this.flagOk = flagOk;
    }

    public FlagConfigModel getFlagNoOk() {
        return flagNoOk;
    }

    public void setFlagNoOk(FlagConfigModel flagNoOk) {
        this.flagNoOk = flagNoOk;
    }
}
