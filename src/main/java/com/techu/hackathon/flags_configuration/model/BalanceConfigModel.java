package com.techu.hackathon.flags_configuration.model;

public class BalanceConfigModel {

    private String sign;

    private double amount;

    public BalanceConfigModel() {
    }

    public BalanceConfigModel(String sign, double amount) {
        this.sign = sign;
        this.amount = amount;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
