package com.techu.hackathon.flags_configuration.controller;

import com.techu.hackathon.flags_configuration.dto.AccountDto;
import com.techu.hackathon.flags_configuration.exception.ResourceException;
import com.techu.hackathon.flags_configuration.model.ConfigurationModel;
import com.techu.hackathon.flags_configuration.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/configuration/flags")
public class ConfigurationController {

    @Autowired
    ConfigurationService service;

    @PostMapping(value = "")
    public ResponseEntity<ConfigurationModel> save(@RequestBody ConfigurationModel model) throws ResourceException {
        ConfigurationModel configuration = service.save(model);
        return new ResponseEntity<>(configuration, HttpStatus.CREATED);
    }

    @GetMapping(value = "")
    public ResponseEntity<List<ConfigurationModel>> findAll(){
        List<ConfigurationModel> configurations = service.findAll();
        return new ResponseEntity<>(configurations, HttpStatus.OK);
    }

    @PutMapping(value = "/{code}")
    public ResponseEntity<ConfigurationModel> update(@PathVariable("code") String code,
                                                     @RequestBody ConfigurationModel model) throws ResourceException{
        ConfigurationModel configuration = service.update(code, model);
        return new ResponseEntity<>(configuration, HttpStatus.CREATED);
    }

    @PostMapping(value = "/accounts")
    public ResponseEntity<Map<String, Integer>> getFlagsByAccounts(@RequestBody List<AccountDto> accounts){
        //System.out.println(accounts.size());
        Map<String, Integer> resultsMap = this.service.getFlagsByAccounts(accounts);
        return new ResponseEntity<>(resultsMap, HttpStatus.OK);
    }
}
