package com.techu.hackathon.flags_configuration.service;

import com.techu.hackathon.flags_configuration.dto.AccountDto;
import com.techu.hackathon.flags_configuration.exception.ResourceException;
import com.techu.hackathon.flags_configuration.model.ConfigurationModel;
import com.techu.hackathon.flags_configuration.repository.ConfigurationRepository;
import com.techu.hackathon.flags_configuration.utils.FlagConditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

    @Autowired
    ConfigurationRepository repository;

    @Override
    public ConfigurationModel save(ConfigurationModel configuration) throws ResourceException {
        if (repository.findByCode(configuration.getFlagCode()) != null) {
            throw new ResourceException("Configuracion encontrada por codigo");
        }
        return repository.save(configuration);
    }

    @Override
    public List<ConfigurationModel> findAll() {
        return repository.findAll();
    }

    @Override
    public ConfigurationModel update(String code, ConfigurationModel configuration) throws ResourceException {
        ConfigurationModel currentConfiguration = repository.findByCode(code);
        if (currentConfiguration == null) {
            if (repository.findByCode(configuration.getFlagCode()) != null) {
                throw new ResourceException("Configuracion no encontrada por codigo");
            }
        }
        configuration.setId(currentConfiguration.getId());
        configuration.setFlagCode(code);
        return repository.update(configuration);
    }

    @Override
    public Map<String, Integer> getFlagsByAccounts(List<AccountDto> accounts) {
        Map<String, Integer> resultsMap = new HashMap<>();
        if (accounts.isEmpty()) {
            return resultsMap;
        }
        List<ConfigurationModel> flagsConfiguration = this.repository.findAll();
        //System.out.println(flagsConfiguration.size());
        for (ConfigurationModel configuration : flagsConfiguration) {
            Map<String, Integer> validationMap = initMap();
            validationMap = FlagConditions.validateProductCondition(configuration.getProductEnabled(), accounts, validationMap, true);
            validationMap = FlagConditions.validateProductCondition(configuration.getProductNotEnabled(), accounts, validationMap, false);
            validationMap = FlagConditions.validateCurrencyCondition(configuration.getCurrencyEnabled(), accounts, validationMap, true);
            validationMap = FlagConditions.validateCurrencyCondition(configuration.getCurrencyNotEnabled(), accounts, validationMap, false);
            validationMap = FlagConditions.validateBalanceCondition(configuration.getBalanceAvailableRequired(), accounts, validationMap, true);
            validationMap = FlagConditions.validateBalanceCondition(configuration.getBalanceAvailableNotRequired(), accounts, validationMap, false);
            resultsMap.put(configuration.getFlagCode(), calculateFlagResult(validationMap));
        }
        return resultsMap;
    }

    private Map<String, Integer> initMap() {
        Map<String, Integer> validationMap = new HashMap<>();
        validationMap.put(FlagConditions.Flag.F_1.getValue(), 0);
        validationMap.put(FlagConditions.Flag.F0.getValue(), 0);
        validationMap.put(FlagConditions.Flag.F1.getValue(), 0);
        return validationMap;
    }

    private int calculateFlagResult(Map<String, Integer> validationMap) {
        if(validationMap.get(FlagConditions.Flag.F0.getValue()) == 0
                && validationMap.get(FlagConditions.Flag.F1.getValue()) == 0){
            return 0;
        }
        if(validationMap.get(FlagConditions.Flag.F0.getValue()) > 0){
            return 0;
        }
        if(validationMap.get(FlagConditions.Flag.F1.getValue()) > 0){
            return 1;
        }
        return 0;
    }
}
