package com.techu.hackathon.flags_configuration.service;

import com.techu.hackathon.flags_configuration.dto.AccountDto;
import com.techu.hackathon.flags_configuration.exception.ResourceException;
import com.techu.hackathon.flags_configuration.model.ConfigurationModel;

import java.util.List;
import java.util.Map;

public interface ConfigurationService {

    ConfigurationModel save(ConfigurationModel configuration) throws ResourceException;

    List<ConfigurationModel> findAll();

    ConfigurationModel update(String code, ConfigurationModel configuration) throws ResourceException;

    Map<String, Integer> getFlagsByAccounts(List<AccountDto> accounts);
}
