package com.techu.hackathon.flags_configuration.utils;

import com.techu.hackathon.flags_configuration.dto.AccountDto;
import com.techu.hackathon.flags_configuration.model.BalanceConfigModel;
import com.techu.hackathon.flags_configuration.model.CurrencyConfigModel;
import com.techu.hackathon.flags_configuration.model.ProductConfigModel;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FlagConditions {

    public static enum Flag {
        F_1("-1"), F0("0"), F1("1");

        private String value;

        Flag(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    static enum SignFlag {
        EQUAL("="), GREATER(">"), LOWER("<");

        private String value;

        SignFlag(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static SignFlag findEnum(String sign) {
            SignFlag result = null;
            for (SignFlag flag : SignFlag.values()) {
                if(flag.getValue().equals(sign)){
                    result = flag;
                    break;
                }
            }
            return result;
        }
    }

    public static Map<String, Integer> validateProductCondition(List<ProductConfigModel> rules, List<AccountDto> accounts,
                                                                Map<String, Integer> validationMap, boolean isEnabled) {
        if (!hasRules(rules)) {
            validationMap.put(Flag.F_1.getValue(), validationMap.get(Flag.F_1.getValue()) + 1);
            return validationMap;
        }

        long filtered = accounts.stream().filter(account -> rules.stream().anyMatch(
                product -> account.getProducto().getNombre().equals(product.getProductoName())))
                .count();

        if (isEnabled && filtered == 0) {
            validationMap.put(Flag.F0.getValue(), validationMap.get(Flag.F0.getValue()) + 1);
            return validationMap;
        }

        if (!isEnabled && filtered > 0) {
            validationMap.put(Flag.F0.getValue(), validationMap.get(Flag.F0.getValue()) + 1);
            return validationMap;
        }

        validationMap.put(Flag.F1.getValue(), validationMap.get(Flag.F1.getValue()) + 1);
        return validationMap;
    }

    public static Map<String, Integer> validateCurrencyCondition(List<CurrencyConfigModel> rules, List<AccountDto> accounts,
                                                                 Map<String, Integer> validationMap, boolean isEnabled) {
        if (!hasRules(rules)) {
            validationMap.put(Flag.F_1.getValue(), validationMap.get(Flag.F_1.getValue()) + 1);
            return validationMap;
        }

        long filtered = accounts.stream().filter(account -> rules.stream().anyMatch(
                currency -> account.getMoneda().equals(currency.getCurrencyCode())))
                .count();

        if (isEnabled && filtered == 0) {
            validationMap.put(Flag.F0.getValue(), validationMap.get(Flag.F0.getValue()) + 1);
            return validationMap;
        }

        if (!isEnabled && filtered > 0) {
            validationMap.put(Flag.F0.getValue(), validationMap.get(Flag.F0.getValue()) + 1);
            return validationMap;
        }

        validationMap.put(Flag.F1.getValue(), validationMap.get(Flag.F1.getValue()) + 1);
        return validationMap;
    }

    public static Map<String, Integer> validateBalanceCondition(List<BalanceConfigModel> rules, List<AccountDto> accounts,
                                                                Map<String, Integer> validationMap, boolean isRequired) {
        if (!hasRules(rules)) {
            validationMap.put(Flag.F_1.getValue(), validationMap.get(Flag.F_1.getValue()) + 1);
            return validationMap;
        }

        long filtered = accounts.stream().filter(account -> rules.stream().anyMatch(
                balance -> validateBalanceBySign(account.getSaldoDisponible(), balance.getAmount(), balance.getSign())))
                .count();

        if (isRequired && filtered == 0) {
            validationMap.put(Flag.F0.getValue(), validationMap.get(Flag.F0.getValue()) + 1);
            return validationMap;
        }

        if (!isRequired && filtered > 0) {
            validationMap.put(Flag.F0.getValue(), validationMap.get(Flag.F0.getValue()) + 1);
            return validationMap;
        }

        validationMap.put(Flag.F1.getValue(), validationMap.get(Flag.F1.getValue()) + 1);
        return validationMap;
    }

    private static boolean hasRules(List rules) {
        return rules != null && rules.size() > 0;
    }

    private static boolean validateBalanceBySign(double currentBalance, double balanceRule, String sign) {
        SignFlag signFlag = SignFlag.findEnum(sign);
        if(signFlag == null){
            System.out.println("Enum not founded");
            return false;
        }
        switch (signFlag){
            case EQUAL:
                return currentBalance == balanceRule;
            case LOWER:
                return currentBalance < balanceRule;
            case GREATER:
                return currentBalance > balanceRule;
        }
        System.out.println("Sign not configure to validate");
        return false;
    }
}
