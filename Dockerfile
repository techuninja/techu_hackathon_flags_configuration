FROM openjdk:11-jre-slim-buster
EXPOSE 8084
ARG JAR_FILE=target/flags_configuration-1.0.0.jar
ADD ${JAR_FILE} flags_configuration.jar
ENTRYPOINT ["java","-jar","/flags_configuration.jar"]